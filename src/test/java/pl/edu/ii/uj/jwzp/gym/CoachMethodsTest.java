package pl.edu.ii.uj.jwzp.gym;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Models.Club;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;
import pl.edu.ii.uj.jwzp.gym.Models.OpeningHours;
import pl.edu.ii.uj.jwzp.gym.Repositories.CoachRepository;
import pl.edu.ii.uj.jwzp.gym.Services.ClubService;
import pl.edu.ii.uj.jwzp.gym.Services.CoachService;
import pl.edu.ii.uj.jwzp.gym.Services.WeeklyEventService;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class CoachMethodsTest {

    @Mock
    CoachRepository coachRepository;

    @Mock
    WeeklyEventService weeklyEventService;

    @Mock
    ClubService clubService;

    CoachService coachService;

    @BeforeEach
    public void init() {
        coachService = new CoachService(coachRepository, weeklyEventService);
    }

    // c3_1L
    @Test
    public void deleteCoach_CannotRemoveCoachWhenIsInEvent_True() {

        coachService.create(new Coach("wojciech", "szlosek", 2000L));

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.WEDNESDAY, new OpeningHours(LocalTime.parse("10:45"), LocalTime.parse("12:05")));
        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("ccc", "Warszawa", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("ccc", "Warszawa", map));

        lenient().when(weeklyEventService.getAllByCoach(1L)).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("11:00"), Duration.ofHours(1), 1L, 1L, 1L, 2L))));
        lenient().when(weeklyEventService.get(1)).thenReturn(new WeeklyEvent(UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("11:00"), Duration.ofHours(1), 1L, 1L, 1L, 2L));

        try {
            coachService.delete(1L);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C3_1")) {
                assertTrue(true);
                return;
            }
        }

        fail("Cannot remove coach with id = 1L, because exist event with this coach!");
    }
}
