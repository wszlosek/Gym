package pl.edu.ii.uj.jwzp.gym;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Models.Club;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;
import pl.edu.ii.uj.jwzp.gym.Models.OpeningHours;
import pl.edu.ii.uj.jwzp.gym.Repositories.ClubRepository;
import pl.edu.ii.uj.jwzp.gym.Services.ClubService;
import pl.edu.ii.uj.jwzp.gym.Services.CoachService;
import pl.edu.ii.uj.jwzp.gym.Services.WeeklyEventService;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ClubMethodsTest {

    @Mock
    ClubRepository clubRepository;

    @Mock
    WeeklyEventService weeklyEventService;

    @Mock
    CoachService coachService;

    ClubService clubService;

    @BeforeEach
    public void init() {
        clubService = new ClubService(clubRepository, weeklyEventService);
    }

    // delete club when club is in event (c2_1)
    @Test
    public void deleteClub_CannotRemoveClubWhenIsInEvent_True() {
        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));
        clubService.create(new Club("klub", "Krakow", map));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach("adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn((new Coach("adam", "kowalski", 2L)));

        lenient().when(weeklyEventService.getAllByClub(1L)).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 1L, 2L))));
        lenient().when(weeklyEventService.get(1)).thenReturn((new WeeklyEvent(UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 1L, 2L)));

        try {
            clubService.delete(1L);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C2_1")) {
                assertTrue(true);
                return;
            }
        }

        fail("Cannot remove club with id = 1, because exist event with this club!");
    }

    // when modifying club's opening hours, events cannot stand out - such change cannot be performed
    @Test
    public void updateClubHours_CannotModifyBecauseEventIsInPreviousClubHours_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));
        clubService.create(new Club(1L, "klub", "Krakow", map));

        lenient().when(clubRepository.findById(1L)).thenReturn(Optional.of(new Club(1L, "klub", "Krakow", map)));
        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach(1L, "adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach(1L, "adam", "kowalski", 2L));

        lenient().when(weeklyEventService.getAllByClub(1L)).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(1L, UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 1L, 2L))));
        lenient().when(weeklyEventService.get(1)).thenReturn(new WeeklyEvent(2L, UUID.randomUUID().toString(),
                DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 1L, 2L));
        var newMap = new HashMap<DayOfWeek, OpeningHours>();
        newMap.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("13:00"), LocalTime.parse("17:00")));
        try {
            clubService.update(new Club(1L, "klub", "Krakow", newMap), 1L);
        } catch (ConstraintBrokenException e) {
            System.out.println(e.toString());
            if (e.getMessage().contains("C5_1")) {
                assertTrue(true);
                return;
            }
        }

        fail("Cannot update this club!");
    }
}
