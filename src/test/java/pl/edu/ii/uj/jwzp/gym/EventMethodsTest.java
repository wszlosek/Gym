package pl.edu.ii.uj.jwzp.gym;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Models.Club;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;
import pl.edu.ii.uj.jwzp.gym.Models.OpeningHours;
import pl.edu.ii.uj.jwzp.gym.Repositories.WeeklyEventRepository;
import pl.edu.ii.uj.jwzp.gym.Services.ClubService;
import pl.edu.ii.uj.jwzp.gym.Services.CoachService;
import pl.edu.ii.uj.jwzp.gym.Services.WeeklyEventService;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class EventMethodsTest {

    @Mock
    WeeklyEventRepository weeklyEventRepository;

    @Mock
    ClubService clubService;

    @Mock
    CoachService coachService;

    WeeklyEventService weeklyEventService;

    @BeforeEach
    public void init() {
        weeklyEventService = new WeeklyEventService(weeklyEventRepository);
    }

    // simple add new event when club exist and coach exist
    @Test
    public void createNewEvent_SuccessfulCreate_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach("adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach("adam", "kowalski", 2L));

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(true);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());

        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 1L, 2L);
        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            fail("Error message: " + e);
        }

        assertTrue(true);
    }

    // try creating event when coach doesn't exist (c3)
    @Test
    public void createNewEvent_CannotCreateBecauseCoachDoesntExist_True() {
        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>());
        lenient().when(coachService.get(1L)).thenReturn(null);

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(false);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());

        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 5L, 10L);

        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C3")) {
                assertTrue(true);
                return;
            }
        }

        fail("Coach with id 1 doesn't exist!");
    }

    // try creating event when club doesn't exist (c2)
    @Test
    public void createNewEvent_CannotCreateBecauseClubDoesntExist_True() {

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach("adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach("adam", "kowalski", 2L));

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>());
        lenient().when(clubService.get(1L)).thenReturn(null);

        lenient().when(coachService.isPresent(1L)).thenReturn(true);
        lenient().when(clubService.isPresent(1L)).thenReturn(false);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());

        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 1L, 1L, 2L, 3L);

        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C2")) {
                assertTrue(true);
                return;
            }
        }

        fail("Club with id 1 doesn't exist!");
    }

    // try creating event when club doesn't exist and coach doesn't exist (c2, c3)
    @Test
    public void createNewEvent_CannotCreateBecauseClubAndCoachDontExist_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>());
        lenient().when(coachService.get(1L)).thenReturn(null);

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(false);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());


        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"), Duration.ofHours(4), 2L, 3L, 5L, 6L);

        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C2")) {
                assertTrue(true);
                return;
            }
        }

        fail("Club with id 2 and Coach with id 3 doesn't exist!");
    }

    // event out of club hours (c4)
    @Test
    public void createNewEvent_CannotCreateBecauseEventIsOutOfClubHours_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));
        map.put(DayOfWeek.SATURDAY, new OpeningHours(LocalTime.parse("00:00"), LocalTime.parse("00:00")));
        map.put(DayOfWeek.SUNDAY, new OpeningHours(LocalTime.parse("00:00"), LocalTime.parse("00:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach("adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach("adam", "kowalski", 2L));

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(true);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());

        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.SATURDAY, LocalTime.parse("11:59"), Duration.ofHours(1), 1L, 1L, 3L, 3L);
        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (!e.toString().contains("C4")) {
                fail("event 1L out of event hours!");
            }
        }

        var event2 = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.WEDNESDAY, LocalTime.parse("12:59"), Duration.ofHours(3), 1L, 1L, 3L, 3L);
        try {
            weeklyEventService.create(event2, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (!e.toString().contains("C4")) {
                fail("event 2 out of event hours!");
            }
        }

        var event3 = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("11:00"), Duration.ofHours(5), 1L, 1L, 0L, 1L);
        try {
            weeklyEventService.create(event3, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (!e.toString().contains("C4")) {
                fail("event 3 out of event hours!");
            }
        }

        var event4 = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("15:50"), Duration.ofMinutes(11), 1L, 1L, 3L, 8L);
        try {
            weeklyEventService.create(event4, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (!e.toString().contains("C4")) {
                fail("event 4 out of event hours!");
            }
        }

        var event5 = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.SUNDAY, LocalTime.parse("00:00"), Duration.ofMinutes(24), 1L, 1L, 2L, 2L);
        try {
            weeklyEventService.create(event5, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (!e.toString().contains("C4")) {
                fail("event 5 out of event hours!");
            }
        }

        assertTrue(true);
    }

    // event longer than 24h (c6)
    @Test
    public void createNewEvent_CannotCreateBecauseEventIsLongerThan24h_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("16:00")));
        map.put(DayOfWeek.SATURDAY, new OpeningHours(LocalTime.parse("00:00"), LocalTime.parse("00:00")));
        map.put(DayOfWeek.SUNDAY, new OpeningHours(LocalTime.parse("00:00"), LocalTime.parse("00:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach("adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach("adam", "kowalski", 2L));

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club("nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club("nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(true);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>());
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>());

        var event1L = new WeeklyEvent(UUID.randomUUID().toString(), DayOfWeek.SATURDAY, LocalTime.parse("12:00"), Duration.ofHours(25), 1L, 1L, 2L, 2L);

        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C6")) {
                assertTrue(true);
                return;
            }
        }

        fail("Event cannot be longer than 24h!");
    }

    // c1L
    @Test
    public void createNewEvents_CannotAddTwoEventsWithCoach2_True() {

        var map = new HashMap<DayOfWeek, OpeningHours>();
        map.put(DayOfWeek.MONDAY, new OpeningHours(LocalTime.parse("12:00"), LocalTime.parse("21:00")));

        lenient().when(coachService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Coach(1L, "adam", "kowalski", 2L))));
        lenient().when(coachService.get(1L)).thenReturn(new Coach(1L,"adam", "kowalski", 2L));

        lenient().when(clubService.getAll(any())).thenReturn(new ArrayList<>(Collections.singleton(new Club(1L,"nazwa", "adres", map))));
        lenient().when(clubService.get(1L)).thenReturn(new Club(1L,"nazwa", "adres", map));

        lenient().when(coachService.isPresent(1L)).thenReturn(true);
        lenient().when(clubService.isPresent(1L)).thenReturn(true);

        var event1L = new WeeklyEvent(1L, UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"),
                Duration.ofHours(4), 1L, 1L, 2L, 5L);
        try {
            weeklyEventService.create(event1L, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            fail("Error message: " + e);
        }

        lenient().when(weeklyEventRepository.findByCoachId(1L)).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(1L, UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"),
                Duration.ofHours(4), 1L, 1L, 2L, 2L))));
        lenient().when(weeklyEventRepository.findByClubId(1L)).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(1L, UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"),
                Duration.ofHours(4), 1L, 1L, 2L, 2L))));
        lenient().when(weeklyEventRepository.findByOrderByDayQualityAsc()).thenReturn(new ArrayList<>(Collections.singleton(new WeeklyEvent(1L, UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("12:00"),
                Duration.ofHours(4), 1L, 1L, 2L, 2L))));

        var event2 = new WeeklyEvent(2L, UUID.randomUUID().toString(), DayOfWeek.MONDAY, LocalTime.parse("14:00"), Duration.ofHours(7), 1L, 1L, 2L, 2L);
        try {
            weeklyEventService.create(event2, clubService, coachService);
        } catch (ConstraintBrokenException e) {
            if (e.toString().contains("C1")) {
                assertTrue(true);
                return;
            }
        }

        fail("Cannot add this event!");
    }
}
