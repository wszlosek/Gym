CREATE TABLE club
(
    id      BIGINT AUTO_INCREMENT NOT NULL,
    name    VARCHAR(255),
    address VARCHAR(255),
    CONSTRAINT pk_club PRIMARY KEY (id)
);

CREATE TABLE coach
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    first_name    VARCHAR(255),
    last_name     VARCHAR(255),
    year_of_birth INT,
    CONSTRAINT pk_coach PRIMARY KEY (id)
);

CREATE TABLE event
(
    id                       BIGINT AUTO_INCREMENT NOT NULL,
    title                    VARCHAR(255),
    time                      time,
    date                      date,
    duration                  BIGINT,
    club_id                  BIGINT,
    coach_id                 BIGINT,
    current_number_of_people BIGINT,
    limit_of_people          BIGINT,
    CONSTRAINT pk_event PRIMARY KEY (id)
);

CREATE TABLE opening_hours
(
    id BIGINT AUTO_INCREMENT NOT NULL,
    _from_ time,
    _to_   time,
    CONSTRAINT pk_opening_hours PRIMARY KEY (id)
);

CREATE TABLE weekly_event
(
    id                       BIGINT AUTO_INCREMENT NOT NULL,
    title                    VARCHAR(255),
    _day_                       INT,
    day_quality              INT,
    time                     time,
    duration                 BIGINT,
    club_id                  BIGINT,
    coach_id                 BIGINT,
    current_number_of_people BIGINT,
    limit_of_people          BIGINT,
    CONSTRAINT pk_weeklyevent PRIMARY KEY (id)
);

CREATE TABLE when_open
(
    club_id          BIGINT NOT NULL,
    opening_hours_id BIGINT NOT NULL,
    when_open_key    INT    NOT NULL,
    CONSTRAINT pk_when_open PRIMARY KEY (club_id, when_open_key)
);

ALTER TABLE when_open
    ADD CONSTRAINT uc_when_open_opening_hours UNIQUE (opening_hours_id);

ALTER TABLE when_open
    ADD CONSTRAINT fk_when_open_on_club FOREIGN KEY (club_id) REFERENCES club (id);

ALTER TABLE when_open
    ADD CONSTRAINT fk_when_open_on_opening_hours FOREIGN KEY (opening_hours_id) REFERENCES opening_hours (id);