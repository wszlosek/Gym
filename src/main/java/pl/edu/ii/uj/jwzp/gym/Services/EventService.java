package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.Constraints;
import pl.edu.ii.uj.jwzp.gym.Exceptions.GeneralException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.Event;
import pl.edu.ii.uj.jwzp.gym.Repositories.EventArchiveRepository;
import pl.edu.ii.uj.jwzp.gym.Repositories.EventRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class EventService {

    private final EventRepository repository;
    private final EventArchiveRepository archive;

    @Autowired
    public EventService(EventRepository repository, EventArchiveRepository archive) {
        this.repository = repository;
        this.archive = archive;
    }

    public EventRepository getRepository() {
        return repository;
    }

    public EventArchiveRepository getArchive() {
        return archive;
    }

    public List<Event> getAllByCoach(Long coachId) {
        return repository.findByCoachId(coachId);
    }

    public List<Event> getAllByClub(Long clubId) {
        return repository.findByClubId(clubId);
    }

    public List<Event> getAllByDate(LocalDate date) {
        return repository.findByDate(date);
    }

    public List<Event> getAll(Example<Event> example, Sort sort) {
        return repository.findAll(example, sort);
    }

    public Page<Event> getAllPaged(Example<Event> example, Pageable pageable) {
        return repository.findAll(example, pageable);
    }

    public Event get(Long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean isPresent(Long id) {
        return repository.existsById(id);
    }

    public boolean condition1(Event event) {
        return repository.findByCoachId(event.getCoachId()).stream()
                .noneMatch(x -> x.getTemporalEventCollection().eventsCollectionsOverlap(event.getTemporalEventCollection())
                        && !Objects.equals(x.getId(), event.getId()));
    }

    public boolean condition2(Event event, ClubService clubService) {
        return clubService.isPresent(event.getClubId());
    }

    public boolean condition3(Event event, CoachService coachService) {
        return coachService.isPresent(event.getCoachId());
    }

    public boolean condition4(Event event, ClubService clubService) {
        var openingHours = clubService.get(event.getClubId()).daysOpeningHours(event.getDate());
        return openingHours.eventsCollectionContains(event.getTemporalEventCollection());
    }

    private void checkCreateConditions(Event event, ClubService clubService, CoachService coachService) throws ConstraintBrokenException {
        if (!condition1(event))
            throw new ConstraintBrokenException(Constraints.C1);
        if (!condition2(event, clubService))
            throw new ConstraintBrokenException(Constraints.C2);
        if (!condition3(event, coachService))
            throw new ConstraintBrokenException(Constraints.C3);
        if (!condition4(event, clubService))
            throw new ConstraintBrokenException(Constraints.C4);
        if (!condition6(event))
            throw new ConstraintBrokenException(Constraints.C6);

        if (event.getCurrentNumber() > event.getPeopleLimit()) {
            throw new ConstraintBrokenException(Constraints.LIMIT);
        }
    }

    public boolean condition6(Event event) {
        return event.getDuration().toHours() <= 24;
    }

    public Event create(Event newEvent, ClubService clubService, CoachService coachService) throws ConstraintBrokenException {
        checkCreateConditions(newEvent, clubService, coachService);
        return repository.save(newEvent);
    }

    public Event archive(Event newEvent) {
        return archive.save(newEvent);
    }

    public Event update(Event newEvent, Long id, ClubService clubService, CoachService coachService) throws ConstraintBrokenException, NotFoundException {
        Event event = get(id);
        event.update(newEvent);

        checkCreateConditions(event, clubService, coachService);
        return repository.save(event);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }
}