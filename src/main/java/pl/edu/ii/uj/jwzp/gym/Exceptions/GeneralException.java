package pl.edu.ii.uj.jwzp.gym.Exceptions;

public class GeneralException extends RuntimeException {
    public GeneralException() {
        super();
    }

    public GeneralException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralException(final String message) {
        super(message);
    }

    public GeneralException(final Throwable cause) {
        super(cause);
    }
}
