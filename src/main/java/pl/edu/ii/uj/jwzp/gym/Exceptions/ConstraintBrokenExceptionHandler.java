package pl.edu.ii.uj.jwzp.gym.Exceptions;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ConstraintBrokenExceptionHandler extends ResponseEntityExceptionHandler {

    @Order(1)
    @ExceptionHandler({ConstraintBrokenException.class})
    public ResponseEntity<Object> handleConstraintBroken(ConstraintBrokenException ex, WebRequest request) {
        ex.printStackTrace();
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Order(2)
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleConstraintBroken(NotFoundException ex, WebRequest request) {
        ex.printStackTrace();
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @Order(3)
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<Object> handleConstraintBroken(RuntimeException ex, WebRequest request) {
        ex.printStackTrace();
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
