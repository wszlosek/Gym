package pl.edu.ii.uj.jwzp.gym.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.edu.ii.uj.jwzp.gym.Exceptions.GeneralException;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "event")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "time")
    private LocalTime time;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "duration")
    private Duration duration;
    @Column(name = "clubId")
    private Long clubId;
    @Column(name = "coachId")
    private Long coachId;

    @Column(name = "currentNumberOfPeople")
    private Long currentNumber;
    @Column(name = "limitOfPeople")
    private Long peopleLimit;

    public Event() {
        this.title = null;
        this.time = null;
        this.date = null;
        this.duration = null;
        this.clubId = null;
        this.coachId = null;
        this.currentNumber = null;
        this.peopleLimit = null;
    }

    public Event(String title, LocalTime time, LocalDate date, Duration duration, Long clubId, Long coachId, Long currentNumber, Long peopleLimit) {
        this.title = title;
        this.time = time;
        this.date = date;
        this.duration = duration;
        this.clubId = clubId;
        this.coachId = coachId;
        this.currentNumber = currentNumber;
        this.peopleLimit = peopleLimit;
    }

    public Event(LocalDate date, Long clubId, Long coachId) {

        this.title = null;
        this.time = null;
        this.date = date;
        this.duration = null;
        this.clubId = clubId;
        this.coachId = coachId;
        this.currentNumber = null;
        this.peopleLimit = null;
    }

    public void update(Event newEvent) {
        if (newEvent.getTitle() != null) setTitle(newEvent.getTitle());
        if (newEvent.getDate() != null) setDate(newEvent.getDate());
        if (newEvent.getTime() != null) setTime(newEvent.getTime());
        if (newEvent.getDuration() != null) setDuration(newEvent.getDuration());
        if (newEvent.getClubId() != null) setClubId(newEvent.getClubId());
        if (newEvent.getCoachId() != null) setCoachId(newEvent.getCoachId());
        if (newEvent.getCurrentNumber() != null) setCurrentNumber(newEvent.getCurrentNumber());
        if (newEvent.getPeopleLimit() != null) setPeopleLimit(newEvent.getPeopleLimit());
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Long getClubId() {
        return clubId;
    }

    public void setClubId(Long clubId) {
        this.clubId = clubId;
    }

    public Long getCoachId() {
        return coachId;
    }

    public void setCoachId(Long coachId) {
        this.coachId = coachId;
    }

    public Long getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(Long currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Long getPeopleLimit() {
        return peopleLimit;
    }

    public void setPeopleLimit(Long peopleLimit) {
        if (peopleLimit < this.getCurrentNumber()) {
            throw new GeneralException("Limit cannot be less than current number of people");
        } else {
            this.peopleLimit = peopleLimit;
        }
    }

    @JsonIgnore
    public LocalDateTime getDateTime() {
        return LocalDateTime.of(date, time);
    }

    @JsonIgnore
    public TemporalEventCollection getTemporalEventCollection() {
        return new TemporalEventCollection(new TemporalEvent(LocalDateTime.of(date, time), duration));
    }

//    @Override
//    public int hashCode() {
//        final int prime = 31;
//        int result = 1;
//        result = prime * result + id.hashCode();
//        result = prime * result + title.hashCode();
//        result = prime * result + time.hashCode();
//        result = prime * result + date.hashCode();
//        result = prime * result + duration.hashCode();
//        result = prime * result + clubId.hashCode();
//        result = prime * result + coachId.hashCode();
//        return result;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj)
//            return true;
//        if (obj == null)
//            return false;
//        if (getClass() != obj.getClass())
//            return false;
//        Event other = (Event) obj;
//        if (!id.equals(other.id))
//            return false;
//        if (!title.equals(other.title))
//            return false;
//        if (!time.equals(other.time))
//            return false;
//        if (!date.equals(other.date))
//            return false;
//        if (!duration.equals(other.duration))
//            return false;
//        if (!clubId.equals(other.clubId))
//            return false;
//        return coachId.equals(other.coachId);
//    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", time=" + time +
                ", date=" + date +
                ", duration=" + duration +
                ", clubId=" + clubId +
                ", coachId=" + coachId +
                ", currentNumber=" + currentNumber +
                ", peopleLimit=" + peopleLimit +
                '}';
    }
}
