package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.Constraints;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.Club;
import pl.edu.ii.uj.jwzp.gym.Repositories.ClubRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class ClubService {

    private final ClubRepository repository;
    private final WeeklyEventService weeklyEventService;

    @Autowired
    public ClubService(ClubRepository repository, WeeklyEventService weeklyEventService) {
        this.repository = repository;
        this.weeklyEventService = weeklyEventService;
    }

    public ClubRepository getRepository() {
        return repository;
    }

    public WeeklyEventService getWeeklyEventService() {
        return weeklyEventService;
    }

    public List<Club> getAll(Sort sort) {
        return repository.findAll(sort);
    }

    public Page<Club> getAllPaged(Pageable p) {
        return repository.findAll(p);
    }

    public Club get(Long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean isPresent(Long id) {
        return repository.existsById(id);
    }

    public Club create(Club newClub) {
        return repository.save(new Club(newClub.getName(), newClub.getAddress(), newClub.getWhenOpen()));
    }

    public Club update(Club newClub, Long id) throws ConstraintBrokenException, NotFoundException {
        var club = get(id);
        club.update(newClub);

        if (!condition5_1(id, club))
            throw new ConstraintBrokenException(Constraints.C5_1);
        return repository.save(club);
    }

    private boolean condition2_1(Long id) {
        return weeklyEventService.getAllByClub(id).isEmpty();
    }

    private boolean condition5_1(long id, Club club) {
        for (var weeklyEvent : weeklyEventService.getAllByClub(id)) {
            var openingHours = club.daysOpeningHours(LocalDate.of(1970, 1, 5));
            if (!openingHours.eventsCollectionContains(weeklyEvent.getTemporalEventCollection())) {
                return false;
            }
        }
        return true;
    }

    public void delete(Long id) throws ConstraintBrokenException {
        if (!condition2_1(id))
            throw new ConstraintBrokenException(Constraints.C2_1);

        repository.deleteById(id);
    }

    public void deleteAll() {
        for (var obj : repository.findAll()) {
            if (condition2_1(obj.getId()))
                repository.deleteById(obj.getId());
        }
    }
}