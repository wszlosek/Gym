package pl.edu.ii.uj.jwzp.gym.Exceptions;

public class ConstraintBrokenException extends GeneralException {

    public ConstraintBrokenException() {
        super();
    }

    public ConstraintBrokenException(final Constraints brokenConstraint, final Throwable cause) {
        super(brokenConstraint.message, cause);
    }

    public ConstraintBrokenException(final Constraints brokenConstraint) {
        super(brokenConstraint.message);
    }

    public ConstraintBrokenException(final Throwable cause) {
        super(cause);
    }
}
