package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;

import java.util.List;

@Service
public class WeeklyEventServiceOverlay {

    private final ClubService clubService;
    private final CoachService coachService;
    private final WeeklyEventService service;

    @Autowired
    public WeeklyEventServiceOverlay(ClubService clubService, CoachService coachService, WeeklyEventService service) {
        this.clubService = clubService;
        this.coachService = coachService;
        this.service = service;
    }

    public ClubService getClubService() {
        return clubService;
    }

    public CoachService getCoachService() {
        return coachService;
    }

    public WeeklyEventService getService() {
        return service;
    }

    public List<WeeklyEvent> getAllByCoach(Long coachId) {
        return service.getAllByCoach(coachId);
    }

    public List<WeeklyEvent> getAllByClub(Long clubId) {
        return service.getAllByClub(clubId);
    }

    public List<WeeklyEvent> getAll(Example<WeeklyEvent> example, Sort sort) {
        return service.getAll(example, sort);
    }

    public Page<WeeklyEvent> getAllPaged(Example<WeeklyEvent> example, Pageable pageable) {
        return service.getAllPaged(example, pageable);
    }

    public WeeklyEvent get(Long id) throws NotFoundException {
        return service.get(id);
    }

    public Boolean isPresent(Long id) {
        return service.isPresent(id);
    }

    public WeeklyEvent create(WeeklyEvent newWeeklyEvent) throws ConstraintBrokenException {
        return service.create(newWeeklyEvent, clubService, coachService);
    }

    public WeeklyEvent update(WeeklyEvent newWeeklyEvent, Long id) throws ConstraintBrokenException, NotFoundException {
        return service.update(newWeeklyEvent, id, clubService, coachService);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public void deleteAll() {
        service.deleteAll();
    }

    public WeeklyEvent addPerson(long id) {
        return service.addPerson(id);
    }
}