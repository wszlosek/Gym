package pl.edu.ii.uj.jwzp.gym.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ii.uj.jwzp.gym.Models.Event;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findByCoachId(Long coachId);

    List<Event> findByClubId(Long clubId);

    List<Event> findByDate(LocalDate date);
}
