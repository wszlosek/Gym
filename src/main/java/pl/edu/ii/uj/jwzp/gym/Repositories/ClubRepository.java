package pl.edu.ii.uj.jwzp.gym.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ii.uj.jwzp.gym.Models.Club;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {
}
