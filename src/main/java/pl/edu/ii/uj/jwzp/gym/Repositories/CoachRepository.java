package pl.edu.ii.uj.jwzp.gym.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;

@Repository
public interface CoachRepository extends JpaRepository<Coach, Long> {
}
