package pl.edu.ii.uj.jwzp.gym.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.time.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Entity
@Table(name = "club")
public class Club extends RepresentationModel<Club> {

    @JsonProperty("whenOpen")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "when_open",
            joinColumns = {@JoinColumn(name = "club_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "opening_hours_id", referencedColumnName = "id")})
    @MapKeyEnumerated
    private Map<DayOfWeek, OpeningHours> whenOpen;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;

    public Club() {
        this.name = null;
        this.address = null;
    }

    public Club(Long id, String name, String address, Map<DayOfWeek, OpeningHours> whenOpen) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.whenOpen = whenOpen
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public Club(String name, String address, Map<DayOfWeek, OpeningHours> whenOpen) {
        this.name = name;
        this.address = address;
        this.whenOpen = whenOpen
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public void update(Club newClub) {
        if (newClub.getName() != null) setName(newClub.getName());
        if (newClub.getAddress() != null) setAddress(newClub.getAddress());
        if (newClub.getWhenOpen() != null) {
            for (var entry : newClub.getWhenOpen().entrySet()) {
                if (whenOpen.containsKey(entry.getKey())) whenOpen.get(entry.getKey()).update(entry.getValue());
                else whenOpen.put(entry.getKey(), entry.getValue());

            }

        }
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<DayOfWeek, OpeningHours> getWhenOpen() {
        return whenOpen;
    }

    public void setWhenOpen(Map<DayOfWeek, OpeningHours> whenOpen) {
        this.whenOpen = whenOpen;
    }

    public TemporalEventCollection daysOpeningHours(LocalDate when) {
        List<TemporalEvent> out = new ArrayList<>();
        when = when.minusDays(when.getDayOfWeek().getValue() - 1);

        for (var entry : whenOpen.entrySet()) {
            addDay(out, entry.getKey(), entry.getValue(), when);
        }
        if (whenOpen.containsKey(DayOfWeek.MONDAY)) {
            addDay(out, DayOfWeek.MONDAY, whenOpen.get(DayOfWeek.MONDAY), when.plusDays(7));
        }
        if (whenOpen.containsKey(DayOfWeek.SUNDAY)) {
            addDay(out, DayOfWeek.SUNDAY, whenOpen.get(DayOfWeek.SUNDAY), when.minusDays(7));
        }
        return new TemporalEventCollection(out);
    }

    @Override
    public String toString() {
        return "Club{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", whenOpen=" + whenOpen +
                '}';
    }

    private void addDay(List<TemporalEvent> list, DayOfWeek dayToAdd, OpeningHours hoursToAdd, LocalDate when) {
        LocalDate dayInTime = when.plusDays(dayToAdd.getValue() - 1);
        if (hoursToAdd.isWholeDay()) {
            list.add(new TemporalEvent(LocalDateTime.of(dayInTime, LocalTime.MIDNIGHT), Duration.ofHours(24)));
        } else {
            list.add(new TemporalEvent(LocalDateTime.of(dayInTime, hoursToAdd.getFrom()), hoursToAdd.howLong()));
        }
    }
}
