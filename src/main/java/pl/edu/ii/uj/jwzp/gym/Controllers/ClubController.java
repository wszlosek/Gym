package pl.edu.ii.uj.jwzp.gym.Controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ii.uj.jwzp.gym.Models.Club;
import pl.edu.ii.uj.jwzp.gym.Services.ClubService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/v2/clubs")
public class ClubController {

    private static final Logger logger = LoggerFactory.getLogger(ClubController.class);

    private final ClubService service;

    @Autowired
    ClubController(ClubService service) {
        this.service = service;
    }

    private void sortByOpeningDay(Club club) {
        club.add(linkTo(methodOn(ClubController.class).get(Math.toIntExact(club.getId()))).withSelfRel());
        club.setWhenOpen(
                club.getWhenOpen()
                        .entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByKey())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new)));
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get all clubs", description = "Returns a list of all clubs.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the club",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Club.class)))})
    })
    //</editor-fold>
    @GetMapping
    public ResponseEntity<?> getAll(
            @SortDefault() Sort sort,
            @RequestParam(defaultValue = "false") boolean pagination,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size
    ) {
        if (pagination) {
            Pageable pageable = PageRequest.of(page, size, sort);
            var clubs = service.getAllPaged(pageable);
            for (var club : clubs.getContent()) {
                sortByOpeningDay(club);
            }
            logger.info("Get all clubs with pagination was called with parameters:" +
                    " sort = " + sort +
                    " page = " + page +
                    " size = " + size);
            return new ResponseEntity<>(clubs, HttpStatus.OK);
        } else {
            var clubs = service.getAll(sort);
            for (var club : clubs) {
                sortByOpeningDay(club);
            }
            logger.info("Get all clubs was called with parameters:" +
                    " sort = " + sort);
            return new ResponseEntity<>(clubs, HttpStatus.OK);
        }
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get a club by its id", description = "Returns a single club.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the club",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Club.class))}),
            @ApiResponse(responseCode = "404", description = "Club not found",
                    content = @Content)})
    //</editor-fold>
    @GetMapping("/{id}")
    public ResponseEntity<?> get(
            @Parameter(description = "Id of a club to be searched")
            @PathVariable(value = "id") int id) {
        Club clubData = service.get((long) id);
        sortByOpeningDay(clubData);

        logger.info("Get club by id = " + id + " - was called: " + clubData);
        return new ResponseEntity<>(clubData, HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Add new club to database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Club successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "505", description = "Failed to add club due to server error",
                    content = @Content)})
    //</editor-fold>
    @PostMapping
    public ResponseEntity<?> create(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Club to be added")
            @RequestBody Club newClub) {
        service.create(newClub);
        logger.info("New club: " + newClub + ", has been added");
        return new ResponseEntity<>("Club successfully added", HttpStatus.CREATED);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Update club data",
            description = "There has to already be a club under given id." +
                    " When modifying club's opening hours, it's events cannot stand out.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Club successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Club update failed due to bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Club not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Club update failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @PatchMapping("/{id}")
    public ResponseEntity<?> update(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Club new data")
            @RequestBody Club newClub,
            @Parameter(description = "Id of a club to be updated") @PathVariable(value = "id") int id) {
        Club club = service.update(newClub, (long) id);

        logger.info("Club with id = " + id + " has been updated to: " + club.toString());
        return new ResponseEntity<>("Club successfully updated", HttpStatus.OK);

    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete club by id",
            description = "Club can only be deleted if it is not connected to any event.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Club successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Club deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(
            @Parameter(description = "id of a club to be deleted")
            @PathVariable(value = "id") int id) {
        service.delete((long) id);

        logger.info("Club with id = " + id + " has been deleted");
        return new ResponseEntity<>("Club successfully deleted", HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete all clubs in database",
            description = "Clubs will only be deleted if they are not connected to any event.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Clubs successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Clubs deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        service.deleteAll();
        logger.info("All possible clubs have been deleted");
        return new ResponseEntity<>("Clubs successfully deleted", HttpStatus.OK);
    }
}
