package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.Constraints;
import pl.edu.ii.uj.jwzp.gym.Exceptions.GeneralException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;
import pl.edu.ii.uj.jwzp.gym.Repositories.WeeklyEventRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class WeeklyEventService {

    private final WeeklyEventRepository repository;

    @Autowired
    public WeeklyEventService(WeeklyEventRepository repository) {
        this.repository = repository;
    }

    public WeeklyEventRepository getRepository() {
        return repository;
    }

    public List<WeeklyEvent> getAllByCoach(Long coachId) {
        return repository.findByCoachId(coachId);
    }

    public List<WeeklyEvent> getAllByClub(Long clubId) {
        return repository.findByClubId(clubId);
    }

    public List<WeeklyEvent> getAll(Example<WeeklyEvent> example, Sort sort) {
        return repository.findAll(example, sort);
    }

    public Page<WeeklyEvent> getAllPaged(Example<WeeklyEvent> example, Pageable pageable) {
        return repository.findAll(example, pageable);
    }

    public WeeklyEvent get(long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean isPresent(Long id) {
        return repository.existsById(id);
    }

    private boolean condition1(WeeklyEvent weeklyEvent) {
        return repository.findByCoachId(weeklyEvent.getCoachId()).stream()
                .noneMatch(c -> c.getTemporalEventCollection().eventsCollectionsOverlap(weeklyEvent.getTemporalEventCollection())
                        && !Objects.equals(c.getId(), weeklyEvent.getId()));
    }

    private boolean condition2(WeeklyEvent weeklyEvent, ClubService clubService) {
        return clubService.isPresent(weeklyEvent.getClubId());
    }

    private boolean condition3(WeeklyEvent weeklyEvent, CoachService coachService) {
        return coachService.isPresent(weeklyEvent.getCoachId());
    }

    private boolean condition4(WeeklyEvent weeklyEvent, ClubService clubService) {
        var openingHours = clubService.get(weeklyEvent.getClubId()).daysOpeningHours(LocalDate.of(1970, 1, 5));
        return openingHours.eventsCollectionContains(weeklyEvent.getTemporalEventCollection());
    }

    private boolean condition6(WeeklyEvent weeklyEvent) {
        return weeklyEvent.getDuration().toHours() <= 24;
    }

    private void checkCreateConditions(WeeklyEvent weeklyEvent, ClubService clubService, CoachService coachService) throws ConstraintBrokenException {
        if (!condition1(weeklyEvent))
            throw new ConstraintBrokenException(Constraints.C1);
        if (!condition2(weeklyEvent, clubService))
            throw new ConstraintBrokenException(Constraints.C2);
        if (!condition3(weeklyEvent, coachService))
            throw new ConstraintBrokenException(Constraints.C3);
        if (!condition4(weeklyEvent, clubService))
            throw new ConstraintBrokenException(Constraints.C4);
        if (!condition6(weeklyEvent))
            throw new ConstraintBrokenException(Constraints.C6);
        if (weeklyEvent.getCurrentNumber() > weeklyEvent.getPeopleLimit()) {
            throw new ConstraintBrokenException(Constraints.LIMIT);
        }
    }

    public WeeklyEvent create(WeeklyEvent newWeeklyEvent, ClubService clubService, CoachService coachService) throws ConstraintBrokenException {
        checkCreateConditions(newWeeklyEvent, clubService, coachService);
        return repository.save(newWeeklyEvent);
    }

    public WeeklyEvent update(WeeklyEvent newWeeklyEvent, Long id, ClubService clubService, CoachService coachService) throws ConstraintBrokenException, NotFoundException {
        WeeklyEvent weeklyEvent = get(id);
        weeklyEvent.update(newWeeklyEvent);

        checkCreateConditions(weeklyEvent, clubService, coachService);
        return repository.save(weeklyEvent);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public WeeklyEvent addPerson(long id) throws NotFoundException {
        var e = get(id);
        if (isPresent(id)) {
            var current = e.getCurrentNumber();
            if (current < e.getPeopleLimit()) {
                e.setCurrentNumber(e.getCurrentNumber() + 1);
                repository.save(e);
            } else {
                throw new GeneralException("Limit has been exhausted.");
            }
        }
        return e;
    }
}