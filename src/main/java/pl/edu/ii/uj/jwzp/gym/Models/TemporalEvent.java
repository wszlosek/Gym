package pl.edu.ii.uj.jwzp.gym.Models;

import java.time.Duration;
import java.time.LocalDateTime;

public record TemporalEvent(LocalDateTime start, Duration duration) {
    public LocalDateTime end() {
        return start.plus(duration);
    }

    public boolean eventsOverlapExclusive(TemporalEvent event2) {
        return (start.isAfter(event2.start) && start.isBefore(event2.end())) ||
                (end().isAfter(event2.start) && end().isBefore(event2.end())) ||
                (start.isBefore(event2.start) && end().isAfter(event2.end())) ||
                (start.isEqual(event2.start) && end().isEqual(event2.end()));
    }

    public boolean eventsOverlapInclusive(TemporalEvent event2) {
        return eventsOverlapExclusive(event2) ||
                start.isEqual(event2.end()) ||
                end().isEqual(event2.start);
    }

    public boolean eventContains(TemporalEvent event2) {
        return (start.isBefore(event2.start) || start.isEqual(event2.start)) &&
                (end().isAfter(event2.end()) || end().isEqual(event2.end()));
    }
}
