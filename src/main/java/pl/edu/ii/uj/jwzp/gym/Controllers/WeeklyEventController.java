package pl.edu.ii.uj.jwzp.gym.Controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;
import pl.edu.ii.uj.jwzp.gym.Services.WeeklyEventServiceOverlay;


@RestController
@RequestMapping("/api/v2/weekly_events")
public class WeeklyEventController {

    private static final Logger logger = LoggerFactory.getLogger(WeeklyEventController.class);

    private final WeeklyEventServiceOverlay service;

    @Autowired
    WeeklyEventController(WeeklyEventServiceOverlay service) {
        this.service = service;
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get all weekly events", description = "Returns a list of all weekly events.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the weekly event",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = WeeklyEvent.class)))})
    })
    //</editor-fold>
    @GetMapping
    public ResponseEntity<?> getAll(
            @Parameter(description = "Coach Id to filter by")
            @RequestParam(required = false) Long coachId,
            @Parameter(description = "Club Id to filter by")
            @RequestParam(required = false) Long clubId,
            @Parameter(description = "Sorting to be applied")
            @SortDefault(sort = "dayQuality", direction = Sort.Direction.ASC) Sort sort,
            @Parameter(description = "Whether to return the data in paged format")
            @RequestParam(defaultValue = "false") boolean pagination,
            @Parameter(description = "Page number, Applicable only if pagination is true")
            @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Page size, Applicable only if pagination is true")
            @RequestParam(defaultValue = "20") int size
    ) {
        Example<WeeklyEvent> example = Example.of(new WeeklyEvent(clubId, coachId));

        if (pagination) {
            Pageable pageable = PageRequest.of(page, size, sort);
            var coaches = service.getAllPaged(example, pageable);
            logger.info("Get all weekly events with pagination was called with parameters:" +
                    " coachId = " + coachId +
                    " clubId = " + clubId +
                    " sort = " + sort +
                    " page = " + page +
                    " size = " + size);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        } else {
            var coaches = service.getAll(example, sort);
            logger.info("Get all weekly events was called with parameters:" +
                    " coachId = " + coachId +
                    " clubId = " + clubId +
                    " sort = " + sort);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        }
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get a weekly event by its id", description = "Returns a single weekly event.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the weekly event",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = WeeklyEvent.class))}),
            @ApiResponse(responseCode = "404", description = "Weekly event not found",
                    content = @Content)})
    //</editor-fold>
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@Parameter(description = "Id of an event to be searched") @PathVariable(value = "id") int id) {

        WeeklyEvent eventData = service.get((long) id);
        logger.info("Get weekly event by id = " + id + " - was called: " + eventData.toString());
        return new ResponseEntity<>(eventData, HttpStatus.OK);
    }


    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Add new weekly event to database",
            description = """
                    Event's club and coach have to exist within database.
                    One coach can be assigned to only one event at any particular time.
                    Events within a club have to be within the club's opening hours.
                    Events cannot be longer than 24h.
                    Number of participants must be lower or equal to limit""")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Weekly event successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Failed to add weekly event due to a bad request",
                    content = @Content),
            @ApiResponse(responseCode = "505", description = "Failed to add weekly event due to a server error",
                    content = @Content)})
    //</editor-fold>
    @PostMapping
    public ResponseEntity<?> create(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Weekly event to be added")
                                    @RequestBody WeeklyEvent newWeeklyEvent) {
        var event = service.create(newWeeklyEvent);
        logger.info("New weekly event: " + event.toString() + ", has been added");
        return new ResponseEntity<>("Event successfully added", HttpStatus.CREATED);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Update event data",
            description = """
                    There has to already be a event under given id.
                    Event's club and coach have to exist within database.
                    One coach can be assigned to only one event at any particular time.
                    Events within a club have to be within the club's opening hours.
                    Events cannot be longer than 24h.
                    Number of participants must be lower or equal to limit.""")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Failed to update event due to a bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Failed to update event due to a server error",
                    content = @Content)})
    //</editor-fold>
    @PatchMapping("/{id}")
    public ResponseEntity<?> update(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Weekly event new data")
                                    @RequestBody WeeklyEvent newWeeklyEvent,
                                    @Parameter(description = "Id of a weekly event to be updated")
                                    @PathVariable(value = "id") int id) {
        var event = service.update(newWeeklyEvent, (long) id);
        logger.info("Weekly event with id = " + id + " has been updated to: " + event.toString());
        return new ResponseEntity<>("Event successfully updated", HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete event by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Event deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Parameter(description = "Id of a weekly event to be deleted")
                                    @PathVariable(value = "id") int id) {
        service.delete((long) id);
        logger.info("Weekly event with id = " + id + " has been deleted");
        return new ResponseEntity<>("Event successfully deleted", HttpStatus.OK);

    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete all events in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Events successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Events deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        service.deleteAll();
        logger.info("All possible weekly events have been deleted");
        return new ResponseEntity<>("Weekly events successfully deleted", HttpStatus.OK);

    }


    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Add new participant to event", description = "Returns a single event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Increment participant number in event",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = WeeklyEvent.class))})
    })
    //</editor-fold>
    @PatchMapping(value = "/{id}/add-person")
    public ResponseEntity<?> addPerson(@Parameter(description = "id of an event where number of people will be incremented")
                                       @PathVariable(value = "id") int id) {
        var a = service.addPerson(id);
        logger.info("Add person to weekly event with id = " + id + " was called");
        return new ResponseEntity<>(a, HttpStatus.OK);
    }
}