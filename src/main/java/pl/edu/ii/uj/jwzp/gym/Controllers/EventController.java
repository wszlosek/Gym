package pl.edu.ii.uj.jwzp.gym.Controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ii.uj.jwzp.gym.Models.Event;
import pl.edu.ii.uj.jwzp.gym.Services.EventServiceOverlay;

import java.time.LocalDate;


@RestController
@RequestMapping("/api/v2/events")
public class EventController {

    private static final Logger logger = LoggerFactory.getLogger(EventController.class);

    private final EventServiceOverlay service;

    @Autowired
    EventController(EventServiceOverlay service) {
        this.service = service;
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get all events", description = "Returns a list of all events.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the event",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Event.class)))})
    })
    //</editor-fold>
    @GetMapping
    public ResponseEntity<?> getAll(
            @Parameter(description = "Date to filter by")
            @RequestParam(required = false) LocalDate date,
            @Parameter(description = "Coach Id to filter by")
            @RequestParam(required = false) Long coachId,
            @Parameter(description = "Club Id to filter by")
            @RequestParam(required = false) Long clubId,
            @Parameter(description = "Sorting to be applied")
            @SortDefault() Sort sort,
            @Parameter(description = "Whether to return the data in paged format")
            @RequestParam(defaultValue = "false") boolean pagination,
            @Parameter(description = "Page number, Applicable only if pagination is true")
            @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Page size, Applicable only if pagination is true")
            @RequestParam(defaultValue = "20") int size
    ) {
        Example<Event> example = Example.of(new Event(date, clubId, coachId));

        if (pagination) {
            Pageable pageable = PageRequest.of(page, size, sort);
            var coaches = service.getAllPaged(example, pageable);
            logger.info("Get all events with pagination was called with parameters:" +
                    " coachId = " + coachId +
                    " clubId = " + clubId +
                    " sort = " + sort +
                    " date = " + date +
                    " page = " + page +
                    " size = " + size);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        } else {
            var coaches = service.getAll(example, sort);
            logger.info("Get all events was called with parameters:" +
                    " coachId = " + coachId +
                    " clubId = " + clubId +
                    " date = " + date +
                    " sort = " + sort);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        }

    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get a event by its id", description = "Returns a single event.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the event",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Event.class))}),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content)})
    //</editor-fold>
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@Parameter(description = "Id of an event to be searched") @PathVariable(value = "id") int id) {
        Event eventData = service.get((long) id);
        logger.info("Get event by id = " + id + " - was called: " + eventData.toString());
        return new ResponseEntity<>(eventData, HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Add new event to database",
            description = """
                    Event's club and coach have to exist within database.
                    One coach can be assigned to only one event at any particular time.
                    Events within a club have to be within the club's opening hours.
                    Events cannot be longer than 24h.
                    Number of participants must be lower or equal to limit.""")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Event successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Failed to add event due to a bad request",
                    content = @Content),
            @ApiResponse(responseCode = "505", description = "Failed to add event due to a server error",
                    content = @Content)})
    //</editor-fold>
    @PostMapping
    public ResponseEntity<?> create(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Event to be added")
                                    @RequestBody Event newEvent) {

        var event = service.create(newEvent);
        logger.info("New event: " + event.toString() + ", has been added");
        return new ResponseEntity<>("Event successfully added", HttpStatus.CREATED);

    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Update event data",
            description = """
                    There has to already be a event under given id.
                    Event's club and coach have to exist within database.
                    One coach can be assigned to only one event at any particular time.
                    Events within a club have to be within the club's opening hours.
                    Events cannot be longer than 24h.
                    Number of participants must be lower or equal to limit.""")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Failed to update event due to a bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Event not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Failed to update event due to a server error",
                    content = @Content)})
    //</editor-fold>
    @PatchMapping("/{id}")
    public ResponseEntity<?> update(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Event new data")
                                    @RequestBody Event newEvent,
                                    @Parameter(description = "Id of a event to be updated")
                                    @PathVariable(value = "id") int id) {
        var event = service.update(newEvent, (long) id);
        logger.info("Event with id = " + id + " has been updated to: " + event.toString());
        return new ResponseEntity<>("Event successfully updated", HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete event by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Event successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Event deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Parameter(description = "id of a event to be deleted")
                                    @PathVariable(value = "id") int id) {
        service.delete((long) id);
        logger.info("Event with id = " + id + " has been deleted");
        return new ResponseEntity<>("Event successfully deleted", HttpStatus.OK);

    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete all events in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Events successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Events deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        service.deleteAll();
        logger.info("All possible events have been deleted");
        return new ResponseEntity<>("Events successfully deleted", HttpStatus.OK);
    }
}