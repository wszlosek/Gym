package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.Event;

import java.time.LocalDate;
import java.util.List;

@Service
public class EventServiceOverlay {

    private final ClubService clubService;
    private final CoachService coachService;
    private final EventService service;

    @Autowired
    public EventServiceOverlay(ClubService clubService, CoachService coachService, EventService service) {
        this.clubService = clubService;
        this.coachService = coachService;
        this.service = service;
    }

    public ClubService getClubService() {
        return clubService;
    }

    public CoachService getCoachService() {
        return coachService;
    }

    public EventService getService() {
        return service;
    }

    public List<Event> getAllByCoach(Long coachId) {
        return service.getAllByClub(coachId);
    }

    public List<Event> getAllByClub(Long clubId) {
        return service.getAllByClub(clubId);
    }

    public List<Event> getAllByDate(LocalDate date) {
        return service.getAllByDate(date);
    }

    public List<Event> getAll(Example<Event> example, Sort sort) {
        return service.getAll(example, sort);
    }

    public Page<Event> getAllPaged(Example<Event> example, Pageable pageable) {
        return service.getAllPaged(example, pageable);
    }

    public Event get(Long id) throws NotFoundException {
        return service.get(id);
    }

    public Boolean isPresent(Long id) {
        return service.isPresent(id);
    }

    public Event create(Event newEvent) throws ConstraintBrokenException {
        return service.create(newEvent, clubService, coachService);
    }

    public Event archive(Event newEvent) {
        return service.archive(newEvent);
    }

    public Event update(Event newEvent, Long id) throws ConstraintBrokenException {
        return service.update(newEvent, id, clubService, coachService);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public void deleteAll() {
        service.deleteAll();
    }
}