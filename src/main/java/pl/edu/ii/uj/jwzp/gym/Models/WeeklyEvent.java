package pl.edu.ii.uj.jwzp.gym.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.edu.ii.uj.jwzp.gym.Exceptions.GeneralException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "weeklyEvent")
public class WeeklyEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "_day_")
    private DayOfWeek day;

    private Integer dayQuality;

    @Column(name = "time")
    private LocalTime time;
    @Column(name = "duration")
    private Duration duration;
    @Column(name = "clubId")
    private Long clubId;
    @Column(name = "coachId")
    private Long coachId;

    @Column(name = "currentNumberOfPeople")
    private Long currentNumber;
    @Column(name = "limitOfPeople")
    private Long peopleLimit;

    public WeeklyEvent() {
        this.title = null;
        this.day = null;
        this.time = null;
        this.duration = null;
        this.clubId = null;
        this.coachId = null;
        this.dayQuality = null;
        this.currentNumber = null;
        this.peopleLimit = null;
    }

    public WeeklyEvent(String title, @NotNull DayOfWeek day, LocalTime time, Duration duration, Long clubId, Long coachId, Long currentNumber, Long peopleLimit) {
        this.title = title;
        this.day = day;
        this.time = time;
        this.duration = duration;
        this.clubId = clubId;
        this.coachId = coachId;
        this.dayQuality = day.getValue();
        this.currentNumber = currentNumber;
        this.peopleLimit = peopleLimit;
    }

    public WeeklyEvent(Long id, String title, @NotNull DayOfWeek day, LocalTime time, Duration duration, Long clubId, Long coachId, Long currentNumber, Long peopleLimit) {
        this.id = id;
        this.title = title;
        this.day = day;
        this.time = time;
        this.duration = duration;
        this.clubId = clubId;
        this.coachId = coachId;
        this.dayQuality = day.getValue();
        this.currentNumber = currentNumber;
        this.peopleLimit = peopleLimit;
    }


    public WeeklyEvent(Long clubId, Long coachId) {
        this.title = null;
        this.day = null;
        this.time = null;
        this.duration = null;
        this.clubId = clubId;
        this.coachId = coachId;
        this.dayQuality = null;
        this.currentNumber = null;
        this.peopleLimit = null;
    }

    public void update(WeeklyEvent newWeeklyEvent) {
        if (newWeeklyEvent.getTitle() != null) setTitle(newWeeklyEvent.getTitle());
        if (newWeeklyEvent.getDay() != null) setDay(newWeeklyEvent.getDay());
        if (newWeeklyEvent.getTime() != null) setTime(newWeeklyEvent.getTime());
        if (newWeeklyEvent.getDuration() != null) setDuration(newWeeklyEvent.getDuration());
        if (newWeeklyEvent.getClubId() != null) setClubId(newWeeklyEvent.getClubId());
        if (newWeeklyEvent.getCoachId() != null) setCoachId(newWeeklyEvent.getCoachId());
        if (newWeeklyEvent.getCurrentNumber() != null) setCurrentNumber(newWeeklyEvent.getCurrentNumber());
        if (newWeeklyEvent.getPeopleLimit() != null) setPeopleLimit(newWeeklyEvent.getPeopleLimit());
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Long getClubId() {
        return clubId;
    }

    public void setClubId(Long clubId) {
        this.clubId = clubId;
    }

    public Long getCoachId() {
        return coachId;
    }

    public void setCoachId(Long coachId) {
        this.coachId = coachId;
    }

    public Long getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(Long currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Long getPeopleLimit() {
        return peopleLimit;
    }

    public void setPeopleLimit(Long peopleLimit) {
        if (peopleLimit < this.getCurrentNumber()) {
            throw new GeneralException("Limit cannot be less than current number of people");
        } else {
            this.peopleLimit = peopleLimit;
        }
    }

    @JsonIgnore
    public TemporalEventCollection getTemporalEventCollection() {
        List<TemporalEvent> out = new ArrayList<>();
        var mondayThen = LocalDate.of(1970, 1, 5);
        var then = mondayThen.plusDays(day.getValue() - 1);
        LocalDateTime accurateTime = LocalDateTime.of(then, time);
        out.add(new TemporalEvent(accurateTime, duration));
        if (accurateTime.plus(duration).isAfter(LocalDateTime.of(mondayThen.plusDays(7), LocalTime.MIDNIGHT))) {
            out.add(new TemporalEvent(accurateTime.minusDays(7), duration));
        }
        return new TemporalEventCollection(out);
    }

    @Override
    public String toString() {
        return "WeeklyEvent{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", day=" + day +
                ", dayQuality=" + dayQuality +
                ", time=" + time +
                ", duration=" + duration +
                ", clubId=" + clubId +
                ", coachId=" + coachId +
                ", currentNumber=" + currentNumber +
                ", peopleLimit=" + peopleLimit +
                '}';
    }
}
