package pl.edu.ii.uj.jwzp.gym.Controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;
import pl.edu.ii.uj.jwzp.gym.Services.CoachService;


@RestController
@RequestMapping("/api/v2/coaches")
public class CoachController {

    private static final Logger logger = LoggerFactory.getLogger(CoachController.class);

    private final CoachService service;

    @Autowired
    CoachController(CoachService service) {
        this.service = service;
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get all coaches", description = "Returns a list of all coaches")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the coaches",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Coach.class)))})
    })
    //</editor-fold>
    @GetMapping
    public ResponseEntity<?> getAll(
            @SortDefault() Sort sort,
            @RequestParam(defaultValue = "false") boolean pagination,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size
    ) {
        if (pagination) {
            Pageable pageable = PageRequest.of(page, size, sort);
            var coaches = service.getAllPaged(pageable);
            logger.info("Get all coaches with pagination was called with parameters:" +
                    " sort = " + sort +
                    " page = " + page +
                    " size = " + size);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        } else {
            var coaches = service.getAll(sort);
            logger.info("Get all coaches was called with parameters:" +
                    " sort = " + sort);
            return new ResponseEntity<>(coaches, HttpStatus.OK);
        }
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Get a coach by its id", description = "Returns a single coach")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the coach",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Coach.class))}),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)})
    //</editor-fold>
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@Parameter(description = "Id of a coach to be searched") @PathVariable(value = "id") int id) {
        Coach coachData = service.get((long) id);

        logger.info("Get coach by id = " + id + " was called and returned: " + coachData.toString());
        return new ResponseEntity<>(coachData, HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Add new coach to database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Coach successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "505", description = "Failed to add coach due to server error",
                    content = @Content)})
    //</editor-fold>
    @PostMapping
    public ResponseEntity<?> create(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Coach to be added")
                                    @RequestBody Coach newCoach) {
        service.create(newCoach);
        logger.info("New coach: " + newCoach.toString() + ", has been added");
        return new ResponseEntity<>("Coach successfully added", HttpStatus.CREATED);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Update coach data",
            description = "There has to already be a coach under given id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Coach successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Coach not found",
                    content = @Content)})
    //</editor-fold>
    @PatchMapping("/{id}")
    public ResponseEntity<?> update(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Coach new data")
                                    @RequestBody Coach newCoach,
                                    @Parameter(description = "Id of a coach to be updated") @PathVariable(value = "id") int id) {
        var coach = service.update(newCoach, (long) id);

        logger.info("Coach with id = " + id + " has been updated to: " + coach.toString());
        return new ResponseEntity<>("Coach successfully updated", HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete coach by id",
            description = "Coach can only be deleted if he is not connected to any event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Coach successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Coach deletion failed due to bad request",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Coach deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Parameter(description = "id of a coach to be deleted.")
                                    @PathVariable(value = "id") int id) {
        service.delete((long) id);

        logger.info("Coach with id = " + id + " has been deleted");
        return new ResponseEntity<>("Coach successfully deleted", HttpStatus.OK);
    }

    //<editor-fold desc="SwaggerConfig">
    @Operation(summary = "Delete all coaches in database",
            description = "Coaches will only be deleted if they are not connected to any event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Coaches successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Coaches deletion failed due to server error",
                    content = @Content)})
    //</editor-fold>
    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        service.deleteAll();
        logger.info("All possible coaches have been deleted");
        return new ResponseEntity<>("Coaches successfully deleted", HttpStatus.OK);
    }
}