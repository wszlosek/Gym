package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Exceptions.Constraints;
import pl.edu.ii.uj.jwzp.gym.Exceptions.NotFoundException;
import pl.edu.ii.uj.jwzp.gym.Models.Coach;
import pl.edu.ii.uj.jwzp.gym.Repositories.CoachRepository;

import java.util.List;

@Service
public class CoachService {

    private final CoachRepository repository;
    private final WeeklyEventService weeklyEventService;

    @Autowired
    public CoachService(CoachRepository repository, WeeklyEventService weeklyEventService) {
        this.repository = repository;
        this.weeklyEventService = weeklyEventService;
    }

    public CoachRepository getRepository() {
        return repository;
    }

    public WeeklyEventService getWeeklyEventService() {
        return weeklyEventService;
    }

    public List<Coach> getAll(Sort sort) {
        return repository.findAll(sort);
    }

    public Page<Coach> getAllPaged(Pageable p) {
        return repository.findAll(p);
    }

    public Coach get(Long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean isPresent(Long id) {
        return repository.existsById(id);
    }

    public Coach create(Coach newCoach) {
        return repository.save(newCoach);
    }

    public Coach update(Coach newCoach, Long id) throws NotFoundException {
        Coach coach = get(id);
        coach.update(newCoach);

        return repository.save(coach);
    }

    private boolean condition3_1(Long id) {
        return weeklyEventService.getAllByCoach(id).isEmpty();
    }

    public void delete(Long id) throws ConstraintBrokenException {
        if (!condition3_1(id))
            throw new ConstraintBrokenException(Constraints.C3_1);

        repository.deleteById(id);
    }

    public void deleteAll() {
        for (var obj : repository.findAll()) {
            if (condition3_1(obj.getId()))
                repository.deleteById(obj.getId());
        }
    }


}