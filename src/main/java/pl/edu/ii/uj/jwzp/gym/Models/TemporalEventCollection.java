package pl.edu.ii.uj.jwzp.gym.Models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public record TemporalEventCollection(List<TemporalEvent> events) {
    public TemporalEventCollection(List<TemporalEvent> events) {
        List<TemporalEvent> myEvents = new ArrayList<>(List.copyOf(events));
        if (!myEvents.isEmpty()) {
            myEvents.sort(Comparator.comparing(TemporalEvent::start));
            TemporalEvent buildEvent = myEvents.get(0);
            List<TemporalEvent> newEvents = new ArrayList<>();
            for (int i = 1; i < myEvents.size(); i++) {
                TemporalEvent next = myEvents.get(i);
                if (buildEvent.eventsOverlapInclusive(next)) {
                    buildEvent = new TemporalEvent(buildEvent.start(), buildEvent.duration().plus(next.duration()));
                } else if (buildEvent.end().plusNanos(1).isEqual(next.start())) {
                    buildEvent = new TemporalEvent(buildEvent.start(), buildEvent.duration().plus(next.duration()).plusNanos(1));
                } else {
                    newEvents.add(buildEvent);
                    buildEvent = new TemporalEvent(next.start(), next.duration());
                }
            }
            newEvents.add(buildEvent);
            this.events = newEvents;
        } else {
            this.events = new ArrayList<>();
        }
    }

    public TemporalEventCollection(TemporalEvent event) {
        this(List.of(event));
    }

    public boolean eventsCollectionsOverlap(TemporalEventCollection eventCollection2) {
        return events.stream().anyMatch(e1 -> eventCollection2.events.stream().anyMatch(e1::eventsOverlapExclusive));
    }

    public boolean eventsCollectionContains(TemporalEventCollection eventCollection2) {
        return eventCollection2.events.stream().allMatch(e2 -> events.stream().anyMatch(e1 -> e1.eventContains(e2)));
    }
}
