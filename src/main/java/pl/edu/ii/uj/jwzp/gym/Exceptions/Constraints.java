package pl.edu.ii.uj.jwzp.gym.Exceptions;


public enum Constraints {
    C1("C1 - One coach can be assigned to only one event at any particular time"),
    C2("C2 -When creating an event, referred club have to exist"),
    C2_1("C2_1 - When removing a club, no event should exist within the club"),
    C3("C3 - When creating an event, referred coach have to exist"),
    C3_1("C3_1 - When removing a coach, no event should be assigned to the coach"),
    C4("C4 - Events within a club have to be within the club's opening hours"),
    C5_1("C5_1 - When modifying club's opening hours, events cannot stand out - such change cannot be performed"),
    C6("C6 - Event cannot be longer than 24h"),
    LIMIT("Current number of people cannot be higher than limit");

    String message;

    Constraints(String message) {
        this.message = message;
    }
}