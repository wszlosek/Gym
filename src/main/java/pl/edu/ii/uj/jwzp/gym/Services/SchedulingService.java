package pl.edu.ii.uj.jwzp.gym.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.edu.ii.uj.jwzp.gym.Exceptions.ConstraintBrokenException;
import pl.edu.ii.uj.jwzp.gym.Models.Event;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;

@Service
public class SchedulingService {

    private final Clock clock;
    private final EventServiceOverlay eventService;
    private final WeeklyEventService weeklyEventService;

    @Autowired
    SchedulingService(WeeklyEventService weeklyEventService, EventServiceOverlay eventService) {
        this.weeklyEventService = weeklyEventService;
        this.eventService = eventService;
        clock = Clock.systemUTC();
    }

    SchedulingService(WeeklyEventService weeklyEventService, EventServiceOverlay eventService, Clock clock) {
        this.weeklyEventService = weeklyEventService;
        this.eventService = eventService;
        this.clock = clock;
    }

    public Clock getClock() {
        return clock;
    }

    public EventServiceOverlay getEventService() {
        return eventService;
    }

    public WeeklyEventService getWeeklyEventService() {
        return weeklyEventService;
    }

    //    @Scheduled(cron = "0 */2 * ? * *") //every 2 minutes
    @Scheduled(cron = "0 0 1 * * ?") // at 01:00 every day
    private void ScheduledActionsTrigger() throws ConstraintBrokenException {
        AutoCreateEvents();
        ArchiveOldEvents();
    }

    private void AutoCreateEvents() throws ConstraintBrokenException {
        for (LocalDate i = LocalDate.now(clock); i.isBefore(LocalDate.now(clock).plusDays(30 + 1)); i = i.plusDays(1)) {
            DayOfWeek dayOfWeek = i.getDayOfWeek();
            for (var event : weeklyEventService.getRepository().findByOrderByDayQualityAsc().stream().filter(x -> x.getDay().equals(dayOfWeek)).toList()) {
                var e = new Event(event.getTitle(), event.getTime(), i, event.getDuration(), event.getClubId(), event.getCoachId(), event.getCurrentNumber(), event.getPeopleLimit());
                if (eventService.getService().getRepository().findByDate(i).stream()
                        .noneMatch(x -> x.getTitle().equals(e.getTitle())
                                && x.getTime().equals(e.getTime()))) {
                    eventService.create(e);
                }
            }
        }
    }

    private void ArchiveOldEvents() throws ConstraintBrokenException {
        for (var eventToDelete : eventService.getService().getRepository().findAll().stream().
                filter(x -> x.getDate().isBefore(LocalDate.now(clock).minusDays(30))).toList()) {
            eventService.archive(eventToDelete);
            eventService.delete(eventToDelete.getId());
        }
    }
}
