package pl.edu.ii.uj.jwzp.gym.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalTime;


@Entity
@Table(name = "opening_hours")
public class OpeningHours {

    @Column(name = "_from_")
    private LocalTime from;
    @Column(name = "_to_")
    private LocalTime to;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    public OpeningHours() {
        from = null;
        to = null;
    }

    public OpeningHours(LocalTime from, LocalTime to) {
        this.from = from;
        this.to = to;
    }

    public void update(OpeningHours newOpeningHours) {
        if (newOpeningHours.getFrom() != null) setFrom(newOpeningHours.getFrom());
        if (newOpeningHours.getTo() != null) setTo(newOpeningHours.getTo());
    }

    public LocalTime getFrom() {
        return from;
    }

    public void setFrom(LocalTime from) {
        this.from = from;
    }

    public LocalTime getTo() {
        return to;
    }

    public void setTo(LocalTime to) {
        this.to = to;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Duration howLong() {
        return Duration.between(from, to);
    }

    @JsonIgnore
    public boolean isWholeDay() {
        return from.equals(to);
    }

    @Override
    public String toString() {
        return "{from: " + from + " to: " + to + "}";
    }
}
