package pl.edu.ii.uj.jwzp.gym.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ii.uj.jwzp.gym.Models.WeeklyEvent;

import java.util.List;

@Repository
public interface WeeklyEventRepository extends JpaRepository<WeeklyEvent, Long> {
    List<WeeklyEvent> findByCoachId(Long coachId);

    List<WeeklyEvent> findByClubId(Long clubId);

    List<WeeklyEvent> findByOrderByDayQualityAsc();

}
